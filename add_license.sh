#!/bin/bash

LICENSE_NOTICE="license-notice.txt"

find . -name "*.go" | while read -r file; do

    if ! grep -q "GNU Affero General Public License" "$file"; then
        echo "Adding license notice to $file"

        temp_file=$(mktemp)

        trap 'rm -f "$temp_file"' EXIT

        (cat "$LICENSE_NOTICE"; echo; cat "$file") > "$temp_file"

        mv "$temp_file" "$file"
    else
        echo "License notice already present in $file"
    fi
done
