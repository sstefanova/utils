#!/bin/env python3
import subprocess
import sys
from pathlib import Path

POETRY_PATH = Path.home() / ".local/bin/poetry"

REPO_URLS = {
    "envvars": "https://gitlab.wikimedia.org/repos/cloud/toolforge/envvars-cli",
    "builds": "https://gitlab.wikimedia.org/repos/cloud/toolforge/builds-cli",
}


def is_poetry_installed():
    """Check if Poetry is installed at the expected path."""
    return POETRY_PATH.exists()


def install_poetry():
    """Install Poetry if it is not already installed."""
    if not is_poetry_installed():
        print("Poetry is not installed. Installing now...")
        subprocess.run(
            "curl -sSL https://install.python-poetry.org | python3 -", shell=True
        )
    else:
        print(f"Poetry is already installed at {POETRY_PATH}")


def clone_or_update_repo(repo_shortname):
    """Clone or update the repository based on the provided short name."""
    repo_url = REPO_URLS.get(repo_shortname)
    if not repo_url:
        print(f"Unknown repository short name: {repo_shortname}")
        sys.exit(1)

    repo_name = Path(repo_url.split("/")[-1])
    repo_path = Path.home() / repo_name

    if repo_path.is_dir():
        print(f"Updating repository in {repo_path}...")
        subprocess.run(["git", "pull"], cwd=repo_path)
    else:
        print(f"Cloning repository from {repo_url} to {repo_path}...")
        subprocess.run(["git", "clone", repo_url, repo_path])
    return repo_path


def switch_branch(repo_path):
    """Switch to the user-specified branch within the cloned repository."""
    subprocess.run(["git", "fetch", "--all"], cwd=repo_path)
    branches = subprocess.run(
        ["git", "branch", "-a"], capture_output=True, cwd=repo_path, text=True
    )
    print("Available branches:")
    print(branches.stdout)
    selected_branch = input("Please enter the branch to switch to: ")
    subprocess.run(["git", "checkout", selected_branch], cwd=repo_path)


def run_poetry_operations(repo_path):
    """Install dependencies using Poetry and spawn a Poetry shell."""
    subprocess.run([str(POETRY_PATH), "install"], cwd=repo_path)
    print("Spawning a shell within the virtual environment. To exit, type 'exit'.")
    subprocess.run([str(POETRY_PATH), "shell"], cwd=repo_path)


def main():
    """Main function to process the CLI argument and manage the repository setup."""
    if len(sys.argv) < 2:
        print("Usage: python script.py <repository_short_name>")
        sys.exit(1)
    repo_shortname = sys.argv[1]
    install_poetry()
    repo_path = clone_or_update_repo(repo_shortname)
    switch_branch(repo_path)
    run_poetry_operations(repo_path)


if __name__ == "__main__":
    main()
